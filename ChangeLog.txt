
--- 2007-03-20, rev 554

- Fixed: When the title of a section is changed, it is not reflected in
  the public copy even after approval.


--- 2007-03-12, 546

- Fixed: In approve, sometimes the difference lines were long and they
  made the buttons Commit and Revert to go out of view (out of
  window).

- Fixed: When a node is deleted, make sure first that it is not
  referenced by other sections, otherwise these sections will become
  invalid.

- In the approve interface, when commiting, a log (message) can be
  added as well.


--- 2007-03-09, rev 542

- The wiki syntax for the images was [<filename<description]
  It was improved to include also the width, like this:
  [<file.png<200<alt - description] or [<file.png<<alt - description]

--- 2007-03-02, rev 537

- Improved the way that media files are handled. After importing a book,
  its media files can be added into the system using the script
  'explode/add_media.sh'. Its first parameter is a tgz archive of media
  (image) files, organized in directories according to the sections
  and subsections that they belong to. The second parameter is the book_id.
  If the book_id is missing then the basename of the archive file with
  everything after the first dot ('.') removed, is taken as the book id.
  The third parameter is the language of the book to which these media
  files will be added; if not given, 'en' is taken by default.


--- 2007-02-27, rev 533

- Improvment: Improved the generation of other formats and downloadables.
  Used 'dblatex' for generating latex and pdf formats. Finally, the
  generation of the PDF formats works fast and well.


--- 2007-02-12, rev 527

- Bugfix: Changed encoding to UTF-8 everywhere (from iso-latin-1); this
          makes DocBookWiki usable for languages with character encoding
          different from iso-latin-1.

- Bugfix: Fixed recursive commit, which was too ineficient due to a bug.


--- 2006-08-20, rev 514

- Improvment: When a book is imploded, images are copied to SVN as well.
              Generating other formats (HTML, PDF, etc.) will now display
              images properly (if the xml file has no errors).


--- 2006-08-19, rev 513

- Bugfix: Fixed recursive commit and revert.


--- 2006-08-12, rev 512

- Improvment: Improved support for media files.


--- 2006-08-06, rev 511

- Improvment: For CDATA sections can be used this text/wiki notation: [[xyz]].
              As a shortcut,
                --code--
                xyz
                ----
              is equivalent to:
                --code
                [[xyz]]
                ----
              The same can be done for --screen, --scr, and --ll.

--- 2006-05-20, rev 493

- Bugfix: Languages at admin interface.


----- r 444 2006-04-13

- Imptovment: Separated the language of the application
              from the language of the content.


--- 2006-03-29, rev 438

- Bugfix: xsltproc-s called by 'cache.sh', sometimes give this error:
          "I/O error : Is a directory"
          This happens in Fedora, but does not happen in SL303.
          This was solved by replacing
              xsltproc -o $cache_path/ \
          by  xsltproc -o $cache_path/unused.ignore \
          in 'content/cache/cache.sh' (thanks to Simos Xenitellis for the
          suggestion).
 
- Bugfix: Removed the variables from the 'match' of <xsl:template>
          (e.g. <xsl:template match="*[@path=$path]">), because the latest
          version of xsltproc gives an error about it.


--- 2005-11-29, rev 426

- Improvment: Replaced nsgmls by xmllint for checking the DocBook validity
              of XML files.


--- 2005-10-12, rev 421

- Fixed: validation problem: warnings (W) should not prevent saving
- Fixed: problem with explode/implode and entities (&amp;amp;lt;)
         and  CDATA sections.


--- 2005-10-10, rev 412

- Fixed: book tags
  The public copy of the book can be fixed at a certain revision or
  date. After that, the modifications in the workspace will not be
  published immediately, even when they are approved. 

  Since it is not easy to remember certain dates or revision numbers,
  the tags can be used, to label a certain revision or date, and to mark
  important points in the development of the document. E.g. a tag can be
  created for each release of the program, if the document is the
  documentation of a program.

  To create a tag for a certain version of the document, first fix the
  book to this version (by revision number or by date), and then
  create a new tag for it, which is like a label for it. Then the tags
  can be selected conveniently from the list of tags.


--- 2005-10-05, rev 409

- Fixed: The path of the SVN is hardcoded and when the application
         is renamed, SVN operations fail. This is fixed by 
         contents/svn_relocate.sh


--- 2005-08-02, rev 392

- Fixed: Added in docbookwiki_guide some instructions 
  for installation of the subversion.

- Fixed: Added some viewcvs installation instructions,
  so that it works with svn as well.

- Improvment: Improves webnotes to:
    + make the moderation optional
    + filter the list of notes according to their status
      (e.g. if the admin wants to see only the notes that
      need to be approved)
    + optionally send email notification to admins 
      when new notes are submited


--- 2005-07-18, rev 391

- Fixed: When the validation of a section is done, it gives errors
  for xref-s to other sections.

- Fixed: Automatic email notification on commit for SVN.

- Improvment: Diffs are displayed in text/wiki mode.


--- 2005-07-07, rev 382

- Improvement: Added a superuser interface for creating/importing
  new books/articles.


--- 2005-07-04, rev 353

- Improvement: xref.php?mode/book_id/node_id/lng
  All the parts are optional and can be ommitted, like this:
    xref.php, xref.php?, xref.php?/, xref.php?/book_id,
    xref.php?admin/book_id, xref.php?edit/book_id/node_id,
    xref.php?approve/book_id//lng, etc.
  'mode can have the values 'view', 'edit', 'approve', 'admin',
  with the default value being 'view'.


--- 2005-07-01, rev 350

- Fixed: xref.php?book_id/node_id

- Improvement: edit.php?book_id/node_id/lng/mode
  node_id, lng and mode can be omitted, like this:
  edit.php?book_id or edit.php?book_id///admin or
  edit.php?book_id//en
  'mode' can be: view, edit, approve, admin, and
  the default is 'view' (if nothing specified).


--- 2005-06-30, rev 340

- Improvement: Menu editing (at superuser interface) was improved so
  that a book can be listed in more than one category (added bookid
  attribute, besides id, which is automatic). Also copy/paste
  functionality was added, to make easier the modification of the
  structure of the menu. Copy is deep, which means that all the branch
  is copied.


--- 2005-06-27, rev 336

- Fixed: <!--comments--> in TextWiki edit mode.

- Improvment: Editing <bookinfo> and <articleinfo> in TextWiki mode.
  Example:
  --info
  @author: Hoxha, Dashamir, dhoxha@inima.al,  [INIMA>http://www.inima.al] 

  @abstract: This article will describe the steps to install 
  and configure a Slackware Router.

  @keywords: linux, slackware, router

  @copyright: Copyright (C) 2005 Dashamir Hoxha.
  ----


--- 2005-06-24, rev 330

- Fixed: On save, the xml/docbook code that is generated by the
  transformation is validated and errors are displayed in a popup window. 


--- 2005-06-21, rev 317

- Improved: i18n of the application (changed messages so that
            they can be translated to other languages).


--- 2005-06-09, rev 298

- Replaced CVS with Subversion.
- Reorganized directory names (conventions):
  {books/,books_html/,books_media/} => books/{svn/,xml/,cache/,media/}book_id/
- Factorised language under book_id: book_id/lng/...


--- 2005-06-01, rev 277

- Added: Automatic email notification on changes
- Improved: documentation for ViewCVS installation
- Added: lcg_install and slackware_router
- Modified: docbookwiki_guide and inima_soft_server
- Added: webnotes (external comments for each node,
         which are not part of the book).
- Added: .cvsignore files, to ignore some generated
         files that are not kept in cvs


--- 2005-05-14, rev 262

- Improved: file ownership and permissions, there is no need 
  for root anymore 


--- 2005-04-11, rev 255

- Fixed: uploading media files
- Fixed: navigation problem in edit


--- 2005-03-30, rev 239

- Fixed: text/wiki prompt/command white space.
- Improved: text/wiki escape special chars: \<, \>
- Improved: docbookwiki_guide
- Fixed: <![CDATA[...]]> and <!--comments--> are now preserved 
  by implode-explode
- Fixed: &entities; in implode-explode
- Improved: separated install commands that need root permissions
  from those that don't need root permissions.


--- 2005-03-06, rev 191

- Convert from text(wiki) into format xml(DocBook);
- Approve interface for the content of a node.
- Removed the root_exec() function and the usage of wrapper,
  in order to improve security. The files that need to be written by apache
  now belong to the group 'apache' and have write permision for the group.
- Integrated with CVS in order to save all the modifications.
- Admin can approve all the changes of a node recursivly.
- The admins can tag the current version of the book (cvs tag),
  revert the whole book to a previous tag or date, etc.
- In the approve interface the user may also check the previous versions
  of the node, revert to a previous version, view the diffs between
  any two revisions etc.
- Integrate somehow with ViewCVS, so that the user can see the different
  revisions of each node and their diffs.
 
- Fixed: If the content of a book does not exist, it gives 
  an error message about it, instead of giving errors, and displays
  a default book (docbookwiki_guide).


--- 2005-01-11, rev 175

- Created an admin interface.
    + manage editors (add, remove, change their password, 
      inspect and change their permissions, etc.)
    + update search indexes, formats, downloadables  etc.
    + editors can change their password

- Each book has at least one admin.
 
- For each node of the book there may be several users that can edit or
  approve it (have edit or approve rights on it). The users that can
  edit a book and their edit rights are decided by the admins.
 
- The admin of a book can edit or approve all the nodes of the book,
  by default. The superuser is considered to be an admin of each book.

- Users can edit or approve only the nodes where they have
  access rights.

- Nodes can be locked before editing, in order to avoid
  concurrent editing and conflicts.

- The color of the buttons Edit and Approve changes according
  to the state of the node: if the node is locked, then the
  Edit button has a dim color, if it is unmodified then the
  Approve button has a 'disabled' color.


--- 2004-10-22, rev 164

- Fixed: Some problems with <![CDATA[ . . . ]]> and <!-- comments -->


--- 2004-09-23, rev 158

- Add tags: <computeroutput>, <userinput>, <replaceable>, <example> etc.
- Added: <footnote>
- Added: Checkbox 'Normalize Space' at Save (edit_content), so that when 
         it is checked, the white space will be normalized, otherwise 
         it will be saved untouched.
- Added: <![CDATA[ . . . ]]> and <!-- comments --> can be used as well.
- Fixed: The title of the section is displayed in the form:
         chapter_title / section_title / subsection_title /
- Changed: The media are saved in hierarchical directories under 'media/',
           similar to xml and html.


--- 2004-09-09, rev 134

- Added: <xref linkend="node_id"/>
- Added: Documents of type 'article' can be handled as well (besides
         the documents of type 'book').
- Fixed: The title of a book is now updated properly.
- Fixed: When exploding, the image filenames have some problems.
- Added: New tags: <acronym>, <literallayout>.


--- 2004-07-28, rev 127

- Fixed: Templates upgraded for the new version of the framework.
- Added: Multimedia files (images) can be handled as well.
- Added: CVS scripts to automate the syncronization with a cvs repository.
- Added: Support for types of ordered lists: <ol type="A"> is transformed
         to <orderedlist numeration="upperalpha">.


--- 2004-07-23, rev 117

- Added: the cvs scripts cvs_update.sh and cvs_commit.sh in content/
- Fixed: Entities (&lt; &gt; etc) are handled better (they are saved
         in the XML file as &amp;lt;  &amp;gt; etc.).


--- 2004-06-22, rev 105

- Different books can be in different languages (not the same set of
  languages for all the books).
- Fixed: When saving in edit mode, it destroys the content.
- Added: <ulink url="http://xyz">abc</ulink> is displayed as
  <a href="http://xyz">abc</a>. Also, <a href=""></a>
  can be used in html edit mode as well.


--- 2004-06-04, rev 92

- Better comments added to xsl files.
- Support for multi language added to xsl files.
- Some basic support for multi-language added to php files.
- When exploding an xml file, id of the book and the language are taken
from the attributes id and lang of the <book> element.
- When the id of a node is changed, the paths of the subnodes are
updated as well (in index.xml).
- Multi language support added.


--- 2004-05-29, rev 79

- id and title are edited separately from the content
- more than one section with the same id are not allowed
- if there are editing errors (in the syntax of xml) they are handled
  more gracefully


--- 2004-05-24, rev 76

- Replaced sect1,sect2,sect3,sect4,sect5 by section.
- Navigation (previous, next nodes) improved.
- Titles are removed from index, they are extracted from content.xml files.
- The menu can be edited online by admin.
- Files subnodes.html, navigation.txt and content.html are placed in a cache
  folder; the folder books/ contains only content.xml and index.xml files.
- Some code optimizations.
- 'wrapper' is used to write files (because apache cannot write).


--- 2004-05-11, rev 69

- Search improved.


--- 2004-05-03, rev 60

- Improved the XSL stylesheets for explode and implode.
- The search now works.
- The structure of the document can be edited: sections
  can be added or deleted, their order can be changed etc.
  The cache files are updated as well for each change.


--- 2004-04-07, rev 33

- Spliting in chunks, according to chapters, sections, subsections, etc.
- Converting a section into HTML for display (using XSL and CSS stylesheets).
- Converting a section into HTML for edit.
- Converting from HTML to XML for save (during edit in HTML mode).
- Converting a section into TEXT and LATEX for edit.
- Automatically generating the other formats of the document (pdf, txt, etc.)