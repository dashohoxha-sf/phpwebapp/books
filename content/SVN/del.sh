#!/bin/bash
### add in svn a new book

### go to this dir
cd $(dirname $0)

### get arguments
if [ "$1" = "" ]
then
  echo "Usage: $0 book_id [lng]"
  echo "where lng is en, de, fr, it, al, etc. (default is en)"
  exit 1;
fi
book_id=$1
lng=${2:-en}

### read the name of the svn sandbox from svn_cfg.txt
if [ ! -f svn_cfg.txt ]; then exit; fi
. svn_cfg.txt

### del from svn the xml file
xml_file=$svn_dir/${book_id}_${lng}.xml
if [ -f $xml_file ]
then
  svn del $xml_file
  svn commit $xml_file -m ''
fi

### del from svn media files
media_dir="$svn_dir/media/$book_id/$lng"    # svn_dir/media/book_id/lng
if [ -d $media_dir ]
then
  svn update $media_dir
  svn del $media_dir
  svn commit $media_dir -m ''
  rm -rf $media_dir
fi

### del the parent dir as well, if empty
media_dir_1=$(dirname $media_dir)
if [ -d $media_dir_1 ] && [ "$(ls $media_dir_1/)" = "" ]
then
  svn update $media_dir_1
  svn del $media_dir_1
  svn commit $media_dir_1 -m ''
  rm -rf $media_dir_1
fi

### delete book from book_list
sed "/^$book_id:$lng/d" -i book_list
