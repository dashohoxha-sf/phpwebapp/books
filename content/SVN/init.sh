#!/bin/bash
### initialise the svn repository

### go to this dir
cd $(dirname $0)

svn_dir='phpwebapp_docs'
repository=$(svn info repo | grep '^URL: ' | cut -d' ' -f2) 

### check that it is not initialized yet
if [ -f svn_cfg.txt ]
then
  echo "$0: It seems to be already initialized, use '$(dirname $0)/clean.sh' first"
  exit 2
fi

### save $svn_dir and $repository in a file for later use
cat <<EOF > svn_cfg.txt
repository='$repository'
svn_dir='$svn_dir'
EOF

### checkout to svn_dir
rm -rf $svn_dir
echo "Checking out '$repository/trunk/' in '$svn_dir'"
svn checkout $repository/trunk/ $svn_dir

### checkout trunk to ../downloads/xml_source/
xml_source=../downloads/xml_source/
rm -rf $xml_source
echo "Checking out '$repository/trunk/' in '$xml_source'"
svn checkout $repository/trunk/ $xml_source
