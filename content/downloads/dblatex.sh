#!/bin/bash
### Convert from xml to formats pdf, latex, ps, using dblatex.
### Has parameters: book_id, format, lng.
### The input file is 'formats/$book_id/$lng/xml/$book_id.$lng.xml'.
### Media files of the book should be in: 'formats/$book_id/$lng/media/'.
### The output is placed in directory: 'formats/$book_id/$lng/$format/'.

### go to this dir
cd $(dirname $0)

function usage
{
  echo "Usage: ${0} book_id [pdf | ps | tex] [lng]"
  exit 1
}

if [ "$1" = "" ]; then usage; fi
echo "--> $0 $1 $2 $3"

### get parameters
book_id=$1
format=$2
lng=${3:-en}

### set variables
book_xml=formats/$book_id/$lng/xml/$book_id.$lng.xml
media=formats/$book_id/$lng/media
output=formats/$book_id/$lng/$format
tmp=formats/tmp
book=$book_id.$lng
dblatexpath=/usr/local/bin/dblatex
dblatex="$dblatexpath -P latex.unicode.use=1 -P imagedata.boxed=1"

### make directories
mkdir -p $output/
mkdir -p $tmp/

### copy the xml file to the temporary directory
cp $book_xml $tmp/$book.xml
ln -s ../$book_id/$lng/media $tmp/

case "$format" in
  pdf ) $dblatex -t pdf -o $output/$book.dblatex.pdf $tmp/$book.xml ;;
  ps  ) $dblatex -t ps  -o $output/$book.dblatex.ps  $tmp/$book.xml ;;
  tex ) $dblatex -t tex -o $output/$book.dblatex.tex $tmp/$book.xml 
        ln -s ../media $output/
        ;;
  *   ) usage ;;
esac

### clean up
rm -rf $tmp/
