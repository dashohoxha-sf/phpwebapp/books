#!/bin/bash
### called by templates/docbook/admin/admin.php
### to regenerate the downloads of a book

### get the parameters
book_id=$1
lng=${2:-en}

### go to this directory
cd $(dirname $0)

### commit the given book
../SVN/commit.sh $book_id $lng

### update xml_source/
svn update xml_source/

### generate the downloads
./make-downloads.sh $book_id $lng

