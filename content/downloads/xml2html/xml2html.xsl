<?xml version='1.0'?><!-- -*-SGML-*- -->
<!--
This file  is part of  DocBookWiki.  DocBookWiki is a  web application
that  displays  and  edits  DocBook  documents.  

Copyright (C) 2004, 2005 Dashamir Hoxha, dashohoxha@users.sf.net

DocBookWiki is free software; you can redistribute it and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

DocBookWiki is  distributed in  the hope that  it will be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with DocBookWiki; if not, write to the Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
-->

<!-- Used to convert quran.xml to html. -->

<xsl:transform xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

<xsl:output method="xml" version="1.0" encoding="utf-8" 
            omit-xml-declaration="yes" standalone="yes" indent="yes" />

<xsl:include href="section-level.xsl" />
<xsl:include href="section.xsl" />

<!-- output the DOCTYPE declaration -->
<xsl:template match="/">
<xsl:text disable-output-escaping="yes"><![CDATA[<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
          "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
]]>
</xsl:text>
<xsl:apply-templates />
</xsl:template>

<!-- match book or article -->
<xsl:template match="book|article">
<xsl:variable name="title">
  <xsl:value-of select="./title | ./bookinfo/title | ./articleinfo/title" />
</xsl:variable>
<html xmlns="http://www.w3.org/1999/xhtml" lang="EN">
<head>
  <title><xsl:value-of select="$title" /></title>  
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <link type="text/css" rel="stylesheet" href="css/styles.css" />
</head>
<body>

  <div class="book-title"><xsl:value-of select="$title" /></div>

  <xsl:apply-templates />

  <!-- append footnotes at the end of the page -->
  <xsl:call-template name="footnotes" />

</body>
</html>
</xsl:template>

<!-- suppress book title -->
<xsl:template match="book/title | article/title 
                    | bookinfo/title | articleinfo/title" />

<!-- match chapter -->
<xsl:template match="bookinfo | articleinfo | preface | appendix
                    | chapter | section | simplesect">
  <xsl:variable name="anchor" select="@id" />
  <a name="{$anchor}" />
  <xsl:variable name="title">
    <xsl:choose>
      <xsl:when test="name(.)='bookinfo' or name(.)='articleinfo'">
	<xsl:value-of select="''" />
      </xsl:when>
      <xsl:when test="name(.)='preface'">
	<xsl:text>Preface: </xsl:text>
	<xsl:value-of select="./title" />
      </xsl:when>
      <xsl:when test="name(.)='appendix'">
	<xsl:text>Appendix: </xsl:text>
	<xsl:value-of select="./title" />
      </xsl:when>
      <xsl:otherwise>
	<xsl:number count="chapter | section | simplesect" from="/" level="multiple" format="1" />
	<xsl:text> - </xsl:text>
	<xsl:value-of select="./title" />
      </xsl:otherwise>
    </xsl:choose>
  </xsl:variable>

  <xsl:variable name="css_class">
    <xsl:apply-templates select="." mode="css_class" />
  </xsl:variable> 

  <div class="{$css_class}">
    <div class="{$css_class}-title">
      <xsl:value-of select="$title" />
    </div>

    <xsl:apply-templates />
  </div>
</xsl:template>

<!-- get the css_class of the chapter or section -->
<xsl:template mode="css_class" match="bookinfo | articleinfo | preface
                                     | appendix| chapter | simplesect">
  <xsl:value-of select="name(.)" />
</xsl:template>
<xsl:template mode="css_class" match="section">
  <xsl:variable name="level">
    <xsl:call-template name="get-section-level" />
  </xsl:variable>
  <xsl:value-of select="concat('sect', $level)" />
</xsl:template>


</xsl:transform>
