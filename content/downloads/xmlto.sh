#!/bin/bash
### Convert to other formats using 'xmlto' (XML docbook + XSL stylesheets).
### Has parameters: book_id, format, lng.
### The input file is 'formats/$book_id/$lng/xml/$book_id.$lng.xml'.
### Media files of the book should be in: 'formats/$book_id/$lng/media/'.
### The output is placed in directory: 'formats/$book_id/$lng/$format/'.

### go to this dir
cd $(dirname $0)

function usage
{
  echo "Usage: ${0} book_id [ html | html1 | pdf | ps | txt ] [lng]"
  exit 1
}

if [ "$1" = "" ]; then usage; fi
echo "--> $0 $1 $2 $3"

### get parameters
book_id=$1
format=$2
lng=${3:-en}

### set variables
book=$book_id.$lng
book_xml=formats/$book_id/$lng/xml/$book.xml
media=formats/$book_id/$lng/media
output=formats/$book_id/$lng/$format
tmp=formats/tmp
xml_file=$tmp/$book.xmlto.xml
stylesheets=/usr/share/sgml/docbook/xsl-stylesheets

### make directories
mkdir -p $output/
mkdir -p $tmp/

### copy the xml file to the temporary directory
cp $book_xml $xml_file
ln -s ../$book_id/$lng/media $tmp/

### generate the requested format
case "$format" in
  html )
     outdir=$output/$book.xmlto/
     mkdir $outdir
     xmlto xhtml -v -o $outdir $xml_file
     #stylesheet=$stylesheets/html/tldp-html-chunk.xsl
     #xmlto html -v -o $outdir -x $stylesheet $xml_file

     ### create a link to the media files of the book
     ln -s ../../media $outdir
     ;;

  html1 )
     outdir=$output/$book.xmlto/
     mkdir $outdir
     xmlto xhtml-nochunks -v -o $outdir $xml_file
     #stylesheet=$stylesheets/html/tldp-html.xsl
     #xmlto html -v -o $outdir -x $stylesheet $xml_file

     ### create a link to the media files of the book
     ln -s ../../media $outdir
     ;;

  pdf )
     xmlto pdf -v -o $output $xml_file
     #stylesheet=$stylesheets/fo/tldp-print.xsl
     #xmlto pdf -v -o $output -x $stylesheet $xml_file
     ;;

  ps  ) xmlto ps -v -o $output $xml_file ;;

  txt ) xmlto txt -v -o $output $xml_file
        ln -s ../../media $outdir
        ;;

  *   ) usage ;;
esac

### clean up
rm -rf $tmp/
