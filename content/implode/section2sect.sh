#!/bin/bash
### convert <section> elements to <sectX> elements

if [ "$1" = "" ]
then
  echo "Usage: $0 file.xml > output.xml"
  echo "Converts <section> elements to <sectX> elements."
  echo "The resulting xml is displayed in the standard output,"
  echo "so it should be redirected to a file."
  exit 1;
fi
file_xml=$1

xslt=$(dirname $0)/../../xsl_transform/implode
xsltproc $xslt/section2sect.xsl $file_xml
