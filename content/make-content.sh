#!/bin/bash
### creates the content of the folders books/, books_html/,
### workspace/books/, etc. by exploding the initial xml files

### go to this dir
cd $(dirname $0)

### create some directories
mkdir -p books/svn workspace initial_xml/uploaded

### make sure that SVN is initialized
if [ ! -f SVN/svn_cfg.txt ]; then SVN/init.sh; fi

### import docbookwiki_guide in the system
./import.sh initial_xml/docbookwiki_guide_en.xml   \
            docbookwiki_guide en                   \
            initial_xml/docbookwiki_guide.media.tgz

### import phpwebapp docs in the system
book_list="phpwebapp_manual
           phpwebapp_tutorial
           phpwebapp_design"
for book_id in $book_list
do
  tar cfz $book_id.media.tgz -C SVN/phpwebapp_docs/media/$book_id/en/ \
      --exclude=.svn .
  ./import.sh SVN/phpwebapp_docs/${book_id}_en.xml \
              $book_id en $book_id.media.tgz
  rm $book_id.media.tgz
done
