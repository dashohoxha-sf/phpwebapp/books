<?php
  /*
   This file is part of DocBookWiki.  DocBookWiki is a web application
   that displays and edits DocBook documents.

   Copyright (C) 2004, 2005, 2006, 2007
   Dashamir Hoxha, dashohoxha@users.sourceforge.net

   DocBookWiki is free software; you can redistribute it and/or modify
   it under the  terms of the GNU General  Public License as published
   by the Free  Software Foundation; either version 2  of the License,
   or (at your option) any later version.

   DocBookWiki is distributed in the  hope that it will be useful, but
   WITHOUT  ANY  WARRANTY;  without   even  the  implied  warranty  of
   MERCHANTABILITY or  FITNESS FOR A PARTICULAR PURPOSE.   See the GNU
   General Public License for more details.

   You should have  received a copy of the  GNU General Public License
   along  with  DocBookWiki;  if  not,  write  to  the  Free  Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA
  */

  /**
   * This file has some general functions.
   */

  /** Execute a shell command, checking it first for invalid input. */
function shell($cmd)
{
  //some basic check for invalid input
  $cmd = ereg_replace(';.*', '', $cmd);
  $cmd .= ' 2>&1';
  $data_owner = DATA_OWNER;
  $output = shell_exec("sudo -u $data_owner $cmd");
  //print "<xmp>sudo -u $data_owner $cmd \n----- \n$output</xmp>\n";  //debug
  return $output;
}

/**
 * Write the content to the file. It is done using a temporary file
 * and copy, in order to belong to the data_owner.
 */
function write_file($fname, &$fcontent)
{
  //create the directories in the path, in case they do not exist
  shell('mkdir -p '.dirname($fname));

  //write the content to a temporary file
  $tmp_fname = write_tmp_file($fcontent);

  //copy the temporary file to the fname
  shell("cp $tmp_fname $fname");

  //delete the temporary file
  unlink($tmp_fname);
}

/**
 * Write the given file content to a temporary file and
 * make the temporary file read-writable by everybody.
 * Return the name of the temporary file.
 */
function write_tmp_file(&$fcontent)
{
  //get the name of a temporary file
  $tmpfile = tempnam('/tmp', 'dbwiki_');

  //open the file and write the content
  $fp = fopen($tmpfile, 'w');
  fputs($fp, $fcontent);
  fclose($fp);

  //make the file read-writeable by everybody
  chmod($tmpfile, 0666);

  //return the name of the file
  return $tmpfile;
}

/**
 * Converts an associative array to a string 
 * like this: key1="value1", key2="value2"
 */
function array2str($assoc_array)
{
  while ( list($key, $value) = each($assoc_array) )
    {
      $str_array[] = "$key=\"$value\"";
    }
  return implode(', ', $str_array);
}
?>