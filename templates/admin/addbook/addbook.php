<?php
  /*
   This file is part of DocBookWiki.  DocBookWiki is a web application
   that displays and edits DocBook documents.

   Copyright (C) 2004, 2005, 2006, 2007
   Dashamir Hoxha, dashohoxha@users.sourceforge.net

   DocBookWiki is free software; you can redistribute it and/or modify
   it under the  terms of the GNU General  Public License as published
   by the Free  Software Foundation; either version 2  of the License,
   or (at your option) any later version.

   DocBookWiki is distributed in the  hope that it will be useful, but
   WITHOUT  ANY  WARRANTY;  without   even  the  implied  warranty  of
   MERCHANTABILITY or  FITNESS FOR A PARTICULAR PURPOSE.   See the GNU
   General Public License for more details.

   You should have  received a copy of the  GNU General Public License
   along  with  DocBookWiki;  if  not,  write  to  the  Free  Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA
  */

  /**
   * This module is used to import a new book/article in the system
   * or to create a new one. A list of the current books in the system
   * is displayed as well and it is also possible to remove some books
   * from the system.
   * Only the superuser can access this module. 
   *
   * @package admin
   * @subpackage addbook
   */
class addbook extends WebObject
{
  function on_create($event_args)
  {
    //running the script may take a long time
    set_time_limit(0);

    //construct an initial xml document, using the parameters of $event_args
    $xml_content = $this->get_initial_xml($event_args);

    //write the xml content to a temporary file
    $tmpfile = write_tmp_file($xml_content);

    //import the xml file
    $bookid = $event_args['bookid'];
    $lng = $event_args['lng'];
    $output = shell(CONTENT."import.sh $tmpfile $bookid $lng");

    //remove the temporary file
    unlink($tmpfile);

    //display the output as debug messages
    WebApp::debug_msg("<xmp>$output</xmp>");
  }

  /** constructs and returns an intial xml document */
  function get_initial_xml($event_args)
  {
    extract($event_args);
    if ($lng=='')  $lng = 'en';

    $xml_author = $this->author_to_xml($author);
    $xml_keywords = $this->keywords_to_xml($keywords);
    $legalnotice = $this->get_legalnotice($author);
    $fname = dirname(__FILE__).'/GNU-FDL.xml';
    $GNU_FDL = file_get_contents($fname);

    if ($doctype=='article')
      {
        $xml_content = "<?xml version='1.0' encoding='utf-8'?>
<!DOCTYPE article PUBLIC \"-//OASIS//DTD DocBook XML V4.2//EN\"
                         \"http://docbook.org/xml/4.2/docbookx.dtd\">
<article id='$bookid' lang='$lng'>
  <title>$title</title>
  <articleinfo id='INFO'>
" . $xml_author . "
    <abstract><para>$abstract</para></abstract>
" . $xml_keywords . $legalnotice . "
  </articleinfo>

  <section id='sect01'><title>Section</title><para/>
    <section id='sect02'><title>Section</title><para/></section>
  </section>
  <section id='sect03'><title>Section</title><para/></section>
  <appendix id='appendix'><title>Appendix</title><para/></appendix>

  $GNU_FDL

</article>
";
      }
    else // $doctype=='book'
      {
        $xml_content = "<?xml version='1.0' encoding='utf-8'?>
<!DOCTYPE book PUBLIC \"-//OASIS//DTD DocBook XML V4.2//EN\"
                      \"http://docbook.org/xml/4.2/docbookx.dtd\">
<book id='$bookid' lang='$lng'>
  <title>$title</title>
  <bookinfo id='INFO'>
" . $xml_author . "
    <abstract><para>$abstract</para></abstract>
" . $xml_keywords . $legalnotice . "
  </bookinfo>

  <preface id='preface'><title>Preface</title><para/></preface>
  <chapter id='chap01'><title>Chapter</title><para/>
    <section id='sect01'><title>Section</title><para/></section>
  </chapter>
   <chapter id='chap02'><title>Chapter</title><para/></chapter>
 <appendix id='appendix'><title>Appendix</title><para/></appendix>

  $GNU_FDL

</book>
";
      }

    return $xml_content;
  }

  /**
   * Gets a string like this: 'Surname, Firstname, email'
   * and returns an xml like this:
   * <author>
   *   <firstname>Firstname</firstname>
   *   <surname>Surname</surname>
   *   <affiliation>
   *     <address><email>email</email></address>
   *   </affiliation>
   * </author>
   */
  function author_to_xml($str)
  {
    $arr = explode(',', $str);
    $surname = $arr[0];
    $firstname = $arr[1];
    $email = $arr[2];

    if ($email=='')  $affiliation = '';
    else
      {
        $affiliation = "
      <affiliation>
        <address><email>$email</email></address>
      </affiliation>";
      }

    $author = "
    <author>
      <firstname>Firstname</firstname>
      <surname>Surname</surname>"
      . $affiliation . "
    </author>
";

    return $author;
  }

  /**
   * Gets a comma separated list of keywords: 'k1, k2, k3,'
   * and returns an xml like this:
   * <keywordset>
   *   <keyword>k1</keyword>
   *   <keyword>k2</keyword>
   *   <keyword>k3</keyword>
   * </keywordset>
   */
  function keywords_to_xml($str)
  {
    $arr = explode(',', $str);
    $keywordset = "    <keywordset>\n";
    for ($i=0; $i < sizeof($arr); $i++)
      {
        $keyword = trim($arr[$i]);
        if ($keyword=='') continue;
        $keywordset .= "      <keyword>$keyword</keyword>\n";
      }
    $keywordset .= "    </keywordset>\n";

    return $keywordset;
  }

  function get_legalnotice($author)
  {
    $arr = explode(',', $author);
    $lastname = $arr[0];
    $firstname = $arr[1];
    $year = date('Y');

    $legalnotice = "
      <legalnotice><para>Copyright (C) $year $firstname $lastname.
      Permission is granted to copy, distribute and/or modify this document
      under the terms of the GNU Free Documentation License, Version 1.1
      or any later version published by the Free Software Foundation;
      with no Invariant Sections, with no Front-Cover Texts, and with no
      Back-Cover Texts. A copy of the license is included in the section
      entitled \"GNU Free Documentation License.\"</para></legalnotice>
";

    return $legalnotice;
  }
}
?>