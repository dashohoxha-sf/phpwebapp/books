// -*-C-*- //tell emacs to use C mode
/*
  This file is part of  DocBookWiki.  DocBookWiki is a web application
  that displays and edits DocBook documents.

  Copyright (C) 2004, 2005, 2006, 2007
  Dashamir Hoxha, dashohoxha@users.sourceforge.net

  DocBookWiki is free software;  you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free  Software Foundation; either  version 2 of the  License, or
  (at your option) any later version.

  DocBookWiki is distributed  in the hope that it  will be useful, but
  WITHOUT  ANY   WARRANTY;  without  even  the   implied  warranty  of
  MERCHANTABILITY or  FITNESS FOR A  PARTICULAR PURPOSE.  See  the GNU
  General Public License for more details.

  You should  have received a copy  of the GNU  General Public License
  along  with  DocBookWiki;  if   not,  write  to  the  Free  Software
  Foundation, Inc., 59 Temple  Place, Suite 330, Boston, MA 02111-1307
  USA
*/

function import_file(fname)
{
  var form = document.bookid;
  var bookid = form.bookid.value;
  var lng = form.lng.value;

  if (bookid=='')
    {
      alert(T_("Please give book id."));
      form.bookid.focus();
      return;
    }

  var event_args = 'book_id='+bookid+';lng='+lng+';fname='+fname;
  SendEvent('import_doc', 'import', event_args);
  alert(T_("Please wait until the book is imported."));
}

function delete_file(fname)
{
  SendEvent('import_doc', 'delete', 'fname='+fname);
}

function upload()
{
  var form = document.upload;
  var xml_file = form.xml_file.value;
  if (! xml_file.match(/\.xml$/))
    {
      alert(T_("The XML file should end in '.xml'."));
      form.xml_file.value = '';
      form.xml_file.fokus();
      return;
    }
  var media_files = form.media_files.value;
  var files = xml_file;
  if (media_files != '')  files = files + ', ' + media_files;
  var uploading = T_("Uploading:");
  var please_wait = T_("Please wait...");
  var win_content = "<html>\n"
    + "<head>\n"
    + " <title>" + uploading + " " + files + " </title>\n"
    + " <style>\n"
    + "   body \n"
    + "   { \n"
    + "     background-color: #f8fff8; \n"
    + "     margin: 10px; \n"
    + "     font-family: sans-serif; \n"
    + "     font-size: 10pt; \n"
    + "     color: #000066; \n"
    + "   } \n"
    + "   h1 { font-size: 12pt; color: #000066; } \n"
    + "   h2 { font-size: 10pt; color: #aa0000; } \n"
    + " </style>\n"
    + "</head>\n"
    + "<body>\n"
    + "  <h1>" + uploading + " " + files + " </h1>\n"
    + "  <h2>" + please_wait + "<h2>\n"
    + "</body>\n"
    + "</html>\n";
  var new_win = window.open('', 'upload_message', 'width=200,height=150');
  new_win.document.write(win_content);

  form.submit();
  SendEvent('import_doc', 'refresh');
}
