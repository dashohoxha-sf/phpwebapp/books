#!/bin/bash

book_id=$1
lng=${2:-en}

content/SVN/commit.sh $book_id $lng
svn update content/downloads/xml_source/${book_id}_${lng}.xml
content/downloads/make-downloads.sh $book_id $lng
