#!/bin/bash
### get the list of the users of the book, in the given range
book_id=$1
first=$2
last=$3
book_access_rights=templates/admin/access_rights/$book_id
if [ -d $book_access_rights ]
then
  ls $book_access_rights | sort | sed -n "$first,${last}p"
fi