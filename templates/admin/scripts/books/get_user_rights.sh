#!/bin/bash
### returns the access rights file for the given user in the given book
book_id=$1
username=$2
book_access_rights=templates/admin/access_rights/$book_id

cat $book_access_rights/$username
