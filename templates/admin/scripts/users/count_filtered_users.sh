#!/bin/bash
### get the number of the filtered user
filter="$1"
users=templates/admin/access_rights/users
gawk -F: "$filter" $users | wc -l
