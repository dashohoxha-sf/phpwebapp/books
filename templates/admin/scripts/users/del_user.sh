#!/bin/bash

username="$1"
access_rights=templates/admin/access_rights

if [ "$username" = "users" ]; then exit; fi

### delete the record of the given user from the list of users
sed -i -n "/^$username:/!p" $access_rights/users

### remove any access rights of the user in the books
find $access_rights/ -type f -name $username | xargs rm
