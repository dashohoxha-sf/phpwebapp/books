#!/bin/bash
### get the filtered users in the given range
filter=$1
first=$2
last=$3
query="$filter {print \$1}"
users=templates/admin/access_rights/users
gawk -F: "$query" $users | sort | sed -n "$first,${last}p"
