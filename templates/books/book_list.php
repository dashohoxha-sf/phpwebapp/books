<?php
/**
 * This file contains an array with the list of books and their titles.
 */
$arr_books = 
array(
  "phpwebapp_manual"   => "WebApp Manual",
  "phpwebapp_tutorial" => "WebApp Tutorial",
  "phpwebapp_design"   => "WebApp Design",
  "docbookwiki_guide"  => "DocBookWiki",
);
?>
