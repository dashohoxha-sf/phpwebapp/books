<?php
  /*
   This file is part of DocBookWiki.  DocBookWiki is a web application
   that displays and edits DocBook documents.

   Copyright (C) 2004, 2005, 2006, 2007
   Dashamir Hoxha, dashohoxha@users.sourceforge.net

   DocBookWiki is free software; you can redistribute it and/or modify
   it under the  terms of the GNU General  Public License as published
   by the Free  Software Foundation; either version 2  of the License,
   or (at your option) any later version.

   DocBookWiki is distributed in the  hope that it will be useful, but
   WITHOUT  ANY  WARRANTY;  without   even  the  implied  warranty  of
   MERCHANTABILITY or  FITNESS FOR A PARTICULAR PURPOSE.   See the GNU
   General Public License for more details.

   You should have  received a copy of the  GNU General Public License
   along  with  DocBookWiki;  if  not,  write  to  the  Free  Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA
  */

include_once dirname(__FILE__).'/edit_funcs.php';

/**
 * Edit the content of the current node.
 *
 * @package docbook
 * @subpackage edit
 */
class edit extends WebObject
{
  function on_set_lock($event_args)
  {
    $lock = $event_args['lock'];
    set_node_lock($lock, 'edit');
  }

  function on_move_up($event_args)
  {
    $node_path = $event_args['node_path'];
    lock_index();
    transform_index('move_up', array('path'=>$node_path));
    updatenavig_moveup($node_path);
    update_subnodes_html_all_langs();
    unlock_index();
  }

  function on_move_down($event_args)
  {
    $node_path = $event_args['node_path'];
    lock_index();
    transform_index('move_down', array('path'=>$node_path));
    updatenavig_movedown($node_path);
    update_subnodes_html_all_langs();
    unlock_index();
  }

  function on_delete($event_args)
  {
    //make sure that the node is not locked by somebody else
    if (locked_by_somebody())  return;

    $node_path = WebApp::getSVar('docbook->node_path');
    if ($node_path=='./' or $node_path=='./INFO/')
      {
        WebApp::message(T_("This node cannot be deleted."));
        return;
      }

    //check that this node is not referenced by any other nodes
    $refs = get_node_references();
    if ($refs != '')
      {
        //display a message
        $msg = T_("This node cannot be deleted because it is referenced by \n  v_refs \nFirst delete the references.");
        $msg = str_replace('v_refs', $refs, $msg);
        WebApp::message($msg);
  
        return;
      }

    lock_index();

    //get the navigations of the node, before removing it
    $navig = get_arr_navigation($node_path);

    //remove it from index
    transform_index('delete', array('path'=>$node_path));

    //update 'navigation.txt' of the neighbour nodes
    update_navigation_all_langs($navig['prev_path']);
    update_navigation_all_langs($navig['next_path']);

    //remove its folders
    delete_removefolders_all_langs($node_path);

    //set state variable docbook->node_path to parent node
    $parent_path = ereg_replace('[^/]+/$', '', $node_path);
    WebApp::setSVar('docbook->node_path', $parent_path);

    //update subnodes.html of the parent and all the ancestors
    update_subnodes_html_all_langs($parent_path);

    unlock_index();
  }

  function on_add_subnode($event_args)
  {
    $id = $event_args['id'];
    $title = $event_args['title'];
    $type = $event_args['type'];

    //check that $id does not already exist in index.xml
    $output = process_index('edit/get_node_id.xsl', array('id'=>$id));
    if ($output==$id)
      {
        $msg = T_("The id 'v_id' is already used by another section.");
        $msg = str_replace('v_id', $id, $msg);
        WebApp::message($msg);
        return;
      }

    lock_index();

    //add a new node to index
    $node_path = WebApp::getSVar('docbook->node_path');
    $params = array('id'=>$id, 'title'=>$title, 
                    'type'=>$type, 'path'=>$node_path);
    transform_index('add_subnode', $params);

    //create a new node and create empty node files
    create_new_node($id, $title, $type);

    //update cache files
    $new_node_path = $node_path.$id.'/';
    update_cache_files_all_langs($new_node_path);

    unlock_index();
  }

  function on_change_id($event_args)
  {
    //make sure that the node is not locked by somebody else
    if (locked_by_somebody())  return;

    $new_id = trim($event_args['id']);

    //validate the new id
    if (!change_id_validate($new_id))  return;

    lock_index();

    $book_id = WebApp::getSVar('docbook->book_id');
    $node_path = WebApp::getSVar('docbook->node_path');
    $new_node_path = ereg_replace('[^/]+/$', $new_id.'/', $node_path);

    //set the new id and path in index.xml
    $params = array('path'=>$node_path, 'new_id'=>$new_id);
    transform_index('update_node', $params);

    //update the state variable docbook->node_path with the new path
    WebApp::setSVar('docbook->node_path', $new_node_path);

    //move folders to the new node path
    changeid_movefolders($node_path, $new_node_path);

    //set the new id in content.xml, for all the languages
    changeid_setid($new_id);

    //update cache files
    changeid_updatecache($new_node_path);

    unlock_index();
  }

  function on_change_title($event_args)
  {
    //make sure that the node is not locked by somebody else
    if (locked_by_somebody())  return;

    lock_index();

    $new_title = $event_args['title'];
    $node_path = WebApp::getSVar('docbook->node_path');

    //set the new title in content.xml
    $xsl_file = XSLT."edit/set_content_title.xsl";
    $params = "--stringparam title \"$new_title\" ";
    $xml_file = file_content_xml(WS_BOOKS, $node_path);
    $content_xml = shell("xsltproc $params $xsl_file $xml_file");
    write_file($xml_file, $content_xml);

    //update cache files
    update_cache_files($node_path);

    //add this node in the list of the modified nodes
    add_to_modified_nodes();

    //set the status of the node to modified
    set_node_status('modified');

    unlock_index();
  }

  function onParse()
  {
    //add node types
    $node_path = WebApp::getSVar('docbook->node_path');
    $node_types = process_index_node('get_node_type');
    list($node_type,$subnode_type) = explode(':', chop($node_types));
    WebApp::addGlobalVars(compact('node_type','subnode_type'));
  }

  function onRender()
  {
    //add variables {{status}}, {{m_user}}, {{m_email}}, {{m_time}}
    $arr_state = get_node_state();
    WebApp::addVar('status', $arr_state['status']);
    $m_user = $arr_state['m_user'];
    $m_email = $arr_state['m_email'];
    WebApp::addVar('m_user', "<a href='mailto:$m_email'>$m_user</a>");
    $m_time = get_date_str($arr_state['m_timestamp']);
    WebApp::addVar('m_time', $m_time);

    //add the variables {{locked_by_somebody}} and {{locked}}
    $locked_by_somebody = locked_by_somebody($arr_state);
    $str_locked_by_somebody = ($locked_by_somebody ? 'true' : 'false');
    $str_locked = (is_locked($arr_state) ? 'true' : 'false');
    WebApp::addVar('locked_by_somebody', $str_locked_by_somebody);
    WebApp::addVar('locked', $str_locked);

    //display a notification message if the node is locked
    if ($locked_by_somebody)
      {
        extract($arr_state);
        $msg = T_("This node is locked for v_mode\n\
by v_l_user (v_l_email).\n\
Please try again later.");
        $msg = str_replace('v_mode', $mode, $msg);
        $msg = str_replace('v_l_user', $l_user, $msg);
        $msg = str_replace('v_l_email', $l_email, $msg);
        WebApp::message($msg);
        WebApp::addVar('l_mode', $mode);
        WebApp::addVar('l_user', "<a href='mailto:$l_email'>$l_user</a>");
        return;
      }

    //add {{id}}
    $node_path = WebApp::getSVar('docbook->node_path');
    ereg('([^/]+)/$', $node_path, $regs);
    $id = $regs[1];
    if ($id=='.')  $id = WebApp::getSVar('docbook->book_id');
    WebApp::addVar("id", $id);

    //add the recordset 'nodetypes'
    $this->add_nodetypes_rs();

    //add the recordset 'subsections'
    add_subsections_rs();
  }

  /**
   * Create and add to webPage the recordset 'node_types' with the types
   * of the nodes that can be added to the current node (preface, appendix,
   * chapter, section, simplesect).
   */
  function add_nodetypes_rs()
  {
    $rs = new EditableRS('node_types');

    $node_type = WebApp::getVar('node_type');
    $subnode_type = WebApp::getVar('subnode_type');
    switch ($node_type)
      {
      case 'book':
	$arr_types = array('chapter', 'appendix', 'preface');
	break;
      case 'article':
	$arr_types = array('section', 'appendix');
	break;
      case 'preface':
      case 'chapter':
      case 'section':
      case 'appendix':
	if ($subnode_type!='')  $arr_types = array($subnode_type);
	else  $arr_types = array('section', 'simplesect');
	break;
      default:
      case 'bookinfo':
      case 'articleinfo':
      case 'simplesect':
	$arr_types = array();
	break;
      }

    for ($i=0; $i < sizeof($arr_types); $i++)
      {
	$t = $arr_types[$i];
	$rs->addRec(array('id' => $t, 'label' => $t));
      }

    global $webPage;
    $webPage->addRecordset($rs);
  }
}
?>