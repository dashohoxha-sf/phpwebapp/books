<?php
  /*
   This file is part of DocBookWiki.  DocBookWiki is a web application
   that displays and edits DocBook documents.

   Copyright (C) 2004, 2005, 2006, 2007
   Dashamir Hoxha, dashohoxha@users.sourceforge.net

   DocBookWiki is free software; you can redistribute it and/or modify
   it under the  terms of the GNU General  Public License as published
   by the Free  Software Foundation; either version 2  of the License,
   or (at your option) any later version.

   DocBookWiki is distributed in the  hope that it will be useful, but
   WITHOUT  ANY  WARRANTY;  without   even  the  implied  warranty  of
   MERCHANTABILITY or  FITNESS FOR A PARTICULAR PURPOSE.   See the GNU
   General Public License for more details.

   You should have  received a copy of the  GNU General Public License
   along  with  DocBookWiki;  if  not,  write  to  the  Free  Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA
  */

  /**
   * Add, rename, delete the media files of the node.
   *
   * @package docbook
   * @subpackage edit
   */
class edit_media extends WebObject
{
  function on_upload($event_args)
  {
    //get the filename of the uploaded file
    $file = $event_args['file'];

    //get the basename of the file
    ereg('([^\\/]*)$', $file, $regs);
    $fname = $regs[1];

    $media_path = $this->get_media_path();
    $file = $media_path.$fname;
    //wait until the file is uploaded
    while (!file_exists($file)) sleep(1);

    //add the file in the repository
    shell("svn add $file");

    //set the status of the node to modified
    set_node_status('modified');
  }

  function on_delete($event_args)
  {
    $media_path = $this->get_media_path();
    $item = $event_args['item'];
    $file = $media_path.$item;
    shell("svn delete --force $file");

    //set the status of the node to modified
    set_node_status('modified');
  }

  function on_rename($event_args)
  {
    $media_path = $this->get_media_path();
    $item = $event_args['item'];
    $new_item = $event_args['new_item'];
    $file = $media_path.$item;
    $new_file = $media_path.$new_item;
    $output = shell("svn status $file");
    if ($output[0]=='A')
      {
        shell("svn revert $file");
        shell("mv $file $new_file");
        shell("svn add $new_file");
      }
    else
      {
        $output = shell("svn move --force $file $new_file");
      }

    //set the status of the node to modified
    set_node_status('modified');
  }

  function onRender()
  {
    $media_path = $this->get_media_path();
    //add the variable {{media_path}}
    WebApp::addVar('media_path', $media_path);

    //build and add the recordset media_items
    //which contains the items of this section
    $rs = new EditableRS('media_items');
    if (file_exists($media_path)) $dir = opendir($media_path);
    if ($dir) 
      {
        while (($file=readdir($dir)) !== false)
          { 
            if ($file=='.' or $file=='..')  continue;
            if (ereg('\\.(xml|txt)$', $file))  continue;
            if (is_dir($file))  continue;
            
            $file_path = $media_path.$file;
            $output = shell("svn status $file_path");
            if ($output[0]=='?')
              {
                shell("rm $file_path");
                continue;
              }

            $rs->addRec(array('item'=>$file));
          }
        closedir($dir); 
      }
    global $webPage;
    $webPage->addRecordset($rs);
  }

  function get_media_path()
  {
    $book_id = WebApp::getSVar('docbook->book_id');
    $lng = WebApp::getSVar('docbook->lng');
    $node_path = WebApp::getSVar('docbook->node_path');
    $media_path = WS_BOOKS."$book_id/$lng/$node_path";

    return $media_path;
  }
}
?>