#!/bin/bash
### called by approve.php to revert the node of a book

### get the parameter
node_dir=$1

### revert the directory non-recursively
svn status -N $node_dir | sed -e 's/^.\{7\}//' | xargs svn revert
