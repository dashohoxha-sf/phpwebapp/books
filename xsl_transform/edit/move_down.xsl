<?xml version='1.0'?><!-- -*-SGML-*- -->
<!--
This file  is part of  DocBookWiki.  DocBookWiki is a  web application
that  displays  and  edits  DocBook  documents.  

Copyright (C) 2004, 2005 Dashamir Hoxha, dashohoxha@users.sf.net

DocBookWiki is free software; you can redistribute it and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

DocBookWiki is  distributed in  the hope that  it will be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with DocBookWiki; if not, write to the Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
-->

<!-- 
Transforms index.xml by exchanging the place of a certain node 
and the following one (if there is a following sibling node).
Is called with a parameter 'path', like this: 
xsltproc -stringparam path $node_path \
         move_down.xsl index.xml
-->

<xsl:transform xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:output method="xml" version="1.0" encoding="utf-8" 
            omit-xml-declaration="no" standalone="no" indent="yes" />


<xsl:template match="preface | appendix | chapter | section | simplesect">
  <xsl:choose>

    <!-- if this section is the selected one and there exists a following -->
    <!-- section, then copy the following section instead of this one     -->
    <xsl:when test="@path=$path and 
                    ( following-sibling::preface | following-sibling::appendix
                    | following-sibling::chapter | following-sibling::section
                    | following-sibling::simplesect )">
      <xsl:copy-of select="following-sibling::*[1]" />
    </xsl:when>

    <!-- if the section previous to this one is the selected one, -->
    <!-- then copy the previous section instead of this one       -->
    <xsl:when test="(preceding-sibling::preface | preceding-sibling::appendix
                     | preceding-sibling::chapter | preceding-sibling::section
                     | preceding-sibling::simplesect)[position()=last()]/@path=$path">
      <xsl:copy-of select="preceding-sibling::*[1]" />
    </xsl:when>

    <!-- otherwise copy this section and apply templates -->
    <xsl:otherwise>
      <xsl:copy>
        <xsl:copy-of select="@*" />
        <xsl:apply-templates />
      </xsl:copy>
    </xsl:otherwise>

  </xsl:choose>
</xsl:template>

<!-- copy everything else -->
<xsl:template match="*|@*">
  <xsl:copy>
    <xsl:apply-templates select="@*" />
    <xsl:apply-templates select="node()" />
  </xsl:copy>
</xsl:template>

</xsl:transform>
