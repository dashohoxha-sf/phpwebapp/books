#!/bin/bash

xml_file=../../../content/books/xml/docbookwiki_guide/en/index.xml
#node_path=./edit_modes/
node_path=./edit_modes/html/html-inline/

# book_dir is relative to xsl file
book_dir=../../content/books/xml/docbookwiki_guide/en/

xsltproc --stringparam book_dir $book_dir \
         --stringparam path "$node_path" \
         ../get_navigation.xsl $xml_file
